<?php

namespace Drupal\poc_module\Plugin\Menu;

use Drupal\Core\Menu\MenuLinkDefault;
use Drupal\Core\Menu\StaticMenuLinkOverridesInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Render\Markup;

class UserProfile extends MenuLinkDefault
{

    /**
     * The current user.
     *
     * @var \Drupal\Core\Session\AccountInterface
     */
    protected $currentUser;


    /**
     * {@inheritdoc}
     */
    public function __construct(array $configuration, $plugin_id, $plugin_definition, StaticMenuLinkOverridesInterface $static_override, AccountInterface $current_user)
    {
        parent::__construct($configuration, $plugin_id, $plugin_definition, $static_override);
        $this->currentUser = $current_user;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
    {
        return new static(
            $configuration,
            $plugin_id,
            $plugin_definition,
            $container->get('menu_link.static.overrides'),
            $container->get('current_user'),
        );
    }

    public function getTitle()
    {
        // return (string) $this->pluginDefinition['title'];
        // Markup::create("<div class='search_filter'><div class='search_container'><a href='#' class='fac-btn ajax_search'>" . t('search') . "</A>");
        return Markup::create('<svg xmlns="http://www.w3.org/2000/svg" height="100%" viewBox="0 0 27 30">
  <g id="Icon_feather-user" data-name="Icon feather-user" transform="translate(-4.5 -3)">
    <path id="Path_1" data-name="Path 1" d="M30,31.5v-3a6,6,0,0,0-6-6H12a6,6,0,0,0-6,6v3" fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
    <path id="Path_2" data-name="Path 2" d="M24,10.5a6,6,0,1,1-6-6A6,6,0,0,1,24,10.5Z" fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
  </g>
</svg>
');
    }
    /**
     * {@inheritdoc}
     */
    public function getCacheContexts()
    {
        return ['user'];
    }

    /**
     * {@inheritdoc}
     */
    public function getRouteParameters()
    {
        // If the user is not Anonymous.
        if (!$this->currentUser->isAnonymous()) {
            // Getting the uid.
            $uid = $this->currentUser->id();
            // Adding the link.
            return ['user' => $uid];
        }
        return [];
    }

}
