<?php

declare(strict_types=1);

namespace Drupal\poc_module\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * poc grid plugin style plugin.
 *
 * @ViewsStyle(
 *   id = "poc_module_poc_grid_plugin",
 *   title = @Translation("poc grid plugin"),
 *   help = @Translation("@todo Add help text here."),
 *   theme = "views_style_poc_module_poc_grid_plugin",
 *   display_types = {"normal"},
 * )
 */
final class PocGridPlugin extends StylePluginBase
{

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;

  /**
   * {@inheritdoc}
   */
  protected $usesRowClass = TRUE;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions(): array
  {
    $options = parent::defineOptions();
    $options['grid_starting_colums'] = ['default' => 4];
    $options['grid_modifier_class'] = ['default' => ''];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state): void
  {
    parent::buildOptionsForm($form, $form_state);
    $form['grid_modifiers_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Grid Modifiers Classes'),
      '#description' => $this->t('The classes that modifiy the style of the grid cards'),
      '#default_value' => $this->options['grid_modifiers_class'],
    ];
    $form['grid_starting_colums'] = [
      '#type' => 'number',
      '#title' => $this->t('Starting Colums Number'),
      '#description' => $this->t('The starting grid columns number'),
      '#default_value' => $this->options['grid_starting_colums'],
    ];
  }

}
