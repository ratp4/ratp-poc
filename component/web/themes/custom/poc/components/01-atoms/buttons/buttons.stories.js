// Buttons Stories
import button from './button.twig';

import buttonData from './button.yml';
import buttonSecondaryData from './button-secondary.yml';
import buttonActionData from './button-action.yml';
import buttonAltData from './button-alt.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Button' };

export const twig = () => button(buttonData);

export const secondaryBtn = () => button(buttonSecondaryData);

export const actionBtn = () => button(buttonActionData);
export const twigAlt = () => button(buttonAltData);
