import MediaQuery from './navModules/MediaQuery';
import Navscrolling from './navModules/Scrolling';
import MobileView from './navModules/MobileView';
import CssRequiredVariables from './navModules/CssRequiredVariables';
import NavSearch from './navModules/NavSearch';
import SubMenu from './navModules/SubMenu';

Drupal.behaviors.mainMenu = {
  attach() {
    const navContainer = document.querySelector('[header-container]');
    if (navContainer) {
      const mobileMediaQuery = 900;
      const navigationSectionHeight = navContainer?.clientHeight;
      const mainMenu = navContainer.querySelector('nav[role="navigation"]');
      const elementsToToggleClass = [navContainer, mainMenu];
      const burgerMenu = navContainer.querySelector('[data-burger-menu]');
      const MobileViewElements = [navContainer, mainMenu, burgerMenu];
      const searchIcon = navContainer.querySelector('[data-search-icon]');
      const searchBlock = navContainer.querySelector('[data-search]');
      const Search = new NavSearch(searchIcon, searchBlock, navContainer);
      Search.init();
      const mediaQueryClasses = new MediaQuery(
        elementsToToggleClass,
        mobileMediaQuery,
      );
      mediaQueryClasses.init();
      const scrollingClasses = new Navscrolling(
        elementsToToggleClass,
        navigationSectionHeight,
      );
      scrollingClasses.init();
      const MobileViewClasses = new MobileView(burgerMenu, MobileViewElements);
      MobileViewClasses.init();

      const RequiredVariables = new CssRequiredVariables(
        elementsToToggleClass,
        mobileMediaQuery,
        navigationSectionHeight,
      );
      RequiredVariables.init();

      const SubMenuClass = new SubMenu(mainMenu);
      SubMenuClass.init();
    }
  },
};
