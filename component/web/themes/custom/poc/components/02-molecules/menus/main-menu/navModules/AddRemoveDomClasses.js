function addClassToElements(elements, classToAdd) {
  elements.forEach((element) => {
    if (!element) return;
    element.classList.add(classToAdd);
  });
}

function removeClassFromElements(elements, classToRemove) {
  elements.forEach((element) => {
    if (!element) return;
    element.classList.remove(classToRemove);
  });
}

function toggleClassesForElements(elements, classToToggle) {
  elements.forEach((element) => {
    if (!element) return;
    element.classList.toggle(classToToggle);
  });
}
module.exports = {
  addClassToElements,
  removeClassFromElements,
  toggleClassesForElements,
};
