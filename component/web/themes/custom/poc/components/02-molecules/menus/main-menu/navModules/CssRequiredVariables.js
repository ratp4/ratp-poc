export default class CssRequiredVariables {
  constructor(elementsToAddPositionCssVars, mediaQueryWidth, menuHeight = 0) {
    if (elementsToAddPositionCssVars && mediaQueryWidth) {
      this.elementsToAddPositionCssVars = elementsToAddPositionCssVars;
      this.menuHeight = menuHeight;
      this.smallDevice = window.matchMedia(`(max-width:${mediaQueryWidth}px)`);
      this.toolbar = document.querySelector('#toolbar-bar');
      this.actionsMenu = document.querySelector('[data-menu].actions');
    }
  }

  init() {
    this.calculateToolBarHeight();
    window.addEventListener('resize', () => {
      this.calculateToolBarHeight();
    });

    this.addRequiredClasses();
    this.smallDevice.addEventListener(
      'change',
      this.addRequiredClasses.bind(this),
    );
  }

  addRequiredClasses() {
    this.elementsToAddPositionCssVars.forEach((element) => {
      element?.style.setProperty('--menu-height', `${this.menuHeight}px`);
      element?.style.setProperty(
        '--action-menu-height',
        `${this.actionsMenu?.clientHeight ?? 0}px`,
      );
    });
  }

  calculateToolBarHeight() {
    if (this.toolbar) {
      this.toolbarPostiton = this.toolbar
        ?.computedStyleMap()
        .get('position').value;
    }

    this.elementsToAddPositionCssVars.forEach((element) => {
      element?.style.setProperty(
        '--toolbar-position',
        `${this.toolbarPostiton ?? 0}`,
      );
      // element?.style.setProperty('--toolbar-height', `${height}px`);
      // element?.style.setProperty('--toolbar-height', `${0}px`);
    });
  }
}
