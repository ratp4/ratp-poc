import { toggleClassesForElements } from './AddRemoveDomClasses';

export default class MediaQuery {
  constructor(elementsToToggleClass, mediaQueryWidth) {
    if (elementsToToggleClass && mediaQueryWidth) {
      this.elementsToToggleClass = elementsToToggleClass;
      this.smallDevice = window.matchMedia(`(max-width:${mediaQueryWidth}px)`);
    }
  }

  init() {
    if (this.smallDevice.matches) {
      toggleClassesForElements(this.elementsToToggleClass, 'mobileView');
    }
    this.smallDevice.addEventListener(
      'change',
      toggleClassesForElements.bind(
        this,
        this.elementsToToggleClass,
        'mobileView',
      ),
    );
  }
}
