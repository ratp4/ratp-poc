export default class MinimizeNavOnScroll {
  initialScroll =
    document.documentElement.scrollY || document.documentElement.scrollTop;

  navContainer = document.querySelector('[header-container]');

  levelOneMenu = this.navContainer?.querySelector('[data-menu-level = "0"]');

  menuHeight = this.navContainer.clientHeight;

  scroll = false;

  constructor() {
    this.navContainer?.style.setProperty(
      '--position',
      `${`${Drupal.displace().top}px`} 0 auto 0`,
    );
    window.addEventListener('scroll', this.handelScrolling.bind(this));
  }

  handelScrolling() {
    const curScroll =
      document.documentElement.scrollY || document.documentElement.scrollTop;
    curScroll > this.menuHeight &&
      curScroll > this.initialScroll &&
      this.scrollDown();
    curScroll === 0 && this.scrollZero();
    // curScroll <= this.initialScroll && this.scrollUp();
    this.initialScroll = curScroll;
  }

  scrollDown() {
    if (this.scroll) return;
    this.navContainer.style.height = '5rem';
    this.levelOneMenu.style.height = '80%';
    this.scroll = true;
  }

  scrollZero() {
    this.scroll = false;
    this.navContainer.style.height = '8rem';
    this.levelOneMenu.style.height = '70%';
  }
}
