import { toggleClassesForElements } from './AddRemoveDomClasses';

export default class MobileView {
  constructor(burgerMenu, elementsToToggleClass = null) {
    if (elementsToToggleClass && burgerMenu) {
      this.elementsToToggleClass = elementsToToggleClass;
      this.burgerMenu = burgerMenu;
    }
  }

  init() {
    this.burgerMenu.addEventListener(
      'click',
      toggleClassesForElements.bind(
        this,
        this.elementsToToggleClass,
        'expanded',
      ),
    );
  }
}
