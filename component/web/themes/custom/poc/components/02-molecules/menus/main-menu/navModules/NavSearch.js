import { toggleClassesForElements } from './AddRemoveDomClasses';

export default class NavSearch {
  constructor(searchIcon, searchBlock, navContainer) {
    if (searchIcon && searchBlock) {
      this.elementsToToggleClass = [searchIcon, searchBlock, navContainer];
      this.navContainer = navContainer;
      this.searchIcon = searchIcon;
      this.searchBlock = searchBlock;
      this.searchInput = searchBlock.querySelector('input[type="search"]');
      this.searchTtitle = searchBlock.querySelector(
        '[data-search-title] .searching',
      );
    }
  }

  init() {
    this.searchIcon?.addEventListener(
      'click',
      this.handelSearchBehaviour.bind(this),
    );
    this.searchInput?.addEventListener(
      'input',
      this.handelSearchInputTyping.bind(this),
    );
  }

  handelSearchInputTyping(e) {
    this.searchTtitle.innerText = e.target.value;
  }

  handelSearchBehaviour() {
    toggleClassesForElements(this.elementsToToggleClass, 'search-expanded');
    const closeSearch = this.searchBlock.querySelector('[data-search-close]');
    closeSearch.addEventListener('click', () => {
      this.navContainer.classList.remove('search-expanded');
      closeSearch.parentElement.classList.remove('search-expanded');
    });
    if (this.searchIcon.classList.contains('search-expanded')) {
      setTimeout(this.searchInput.focus(), 1000);
      this.searchInput.focus();
    }
  }
}
