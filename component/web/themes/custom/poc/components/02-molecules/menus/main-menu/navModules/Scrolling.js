import {
  addClassToElements,
  removeClassFromElements,
  // toggleClassesForElements,
} from './AddRemoveDomClasses';

export default class Scrolling {
  constructor(elementsToToggleClass = null, navigationSectionHeight = 0) {
    this.elementsToToggleClass = elementsToToggleClass;
    if (elementsToToggleClass) {
      this.navigationSectionHeight = navigationSectionHeight;
      this.initialScroll =
        document.documentElement.scrollY || document.documentElement.scrollTop;
      this.scroll = false;
    }
  }

  init() {
    if (this.initialScroll <= this.navigationSectionHeight) {
      addClassToElements(this.elementsToToggleClass, 'scrollZero');
    }
    window.addEventListener('scroll', this.handelScrolling.bind(this));
  }

  handelScrolling() {
    const curScroll =
      document.documentElement.scrollY || document.documentElement.scrollTop;
    if (
      curScroll > this.navigationSectionHeight &&
      curScroll > this.initialScroll
    ) {
      this.scrollDown();
    }
    if (curScroll === 0) {
      this.scrollZero();
    }
    // curScroll <= this.initialScroll && this.scrollUp();
    this.initialScroll = curScroll;
  }

  scrollDown() {
    if (!this.scroll) {
      addClassToElements(this.elementsToToggleClass, 'scrollDown');
      removeClassFromElements(this.elementsToToggleClass, 'scrollZero');
      this.scroll = true;
    }
  }

  scrollZero() {
    this.scroll = false;
    addClassToElements(this.elementsToToggleClass, 'scrollZero');
    removeClassFromElements(this.elementsToToggleClass, 'scrollDown');
  }
}
