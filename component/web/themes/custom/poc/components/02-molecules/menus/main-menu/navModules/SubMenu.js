export default class SubMenu {
  constructor(mainMenu) {
    this.mainMenu = mainMenu;
  }

  init() {
    const firstLevelMenuItemsWithChildrens = this.mainMenu.querySelectorAll(
      '[data-menu-level="0"] [data-menu-item][data-has-children="true"]',
    );
    const subMenus = document.querySelectorAll('[data-menu-level="1"]');
    subMenus.forEach((el) => {
      el?.style.setProperty('--sub-menu_height', `${el.scrollHeight}px`);
    });
    firstLevelMenuItemsWithChildrens.forEach((item) => {
      item.addEventListener('click', (e) => {
        e.preventDefault();
        const nestedMenu = item.querySelector('[data-menu]');
        const subMenuCloseButton =
          nestedMenu.querySelector('[data-menu-close]');
        const subMenuTitle = nestedMenu.querySelector('[data-menu-title]');
        subMenuTitle.innerText = item.children[0].innerText;
        item.classList.toggle('expanded');
        nestedMenu?.classList.toggle('open');
        // nestedMenu?.style.setProperty(
        //   '--sub-menu_height',
        //   `${nestedMenu.scrollHeight}px`,
        // );

        subMenuCloseButton.addEventListener('click', (el) => {
          el.parentElement.classList.remove('open');
        });
      });
    });
  }
}
