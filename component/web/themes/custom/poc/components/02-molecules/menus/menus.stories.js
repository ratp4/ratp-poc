// import mainMenu from './main-menu/main-menu.twig';

// import mainMenuData from './main-menu/main-menu.yml';

// import './main-menu/main-menu';

// /**
//  * Storybook Definition.
//  */
// export default { title: 'Molecules/Menus' };

// export const main = () => mainMenu(mainMenuData);
import mainMenu from './main-menu/_menu.twig';

import mainMenuData from './main-menu/main-menu.yml';

import './main-menu/main-menu';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Menus' };

export const main = () => mainMenu(mainMenuData);
