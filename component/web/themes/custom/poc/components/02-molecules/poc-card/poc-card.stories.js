import card from './card.twig';

import cardData from './card.yml';
import cardVariant from './card-bg.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/poc-card' };

export const cardExample = () => card(cardData);

export const cardVariant1 = () => card({ ...cardData, ...cardVariant });
