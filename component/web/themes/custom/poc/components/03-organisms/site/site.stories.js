import siteHeader from './site-header/site-header.twig';
import siteHeaderData from './site-header/site-header.yml';
import mainMenuData from '../../02-molecules/menus/main-menu/main-menu.yml';

import '../../02-molecules/menus/main-menu/main-menu';

/**
 * Storybook Definition.
 */
export default {
  title: 'Organisms/Site',
  parameters: {
    layout: 'fullscreen',
  },
};
export const header = () =>
  siteHeader({
    ...siteHeaderData,
    ...mainMenuData,
  });
